googledrive_doc_viewer is an extension to the core field module
which must be enabled.

1. Unpack the googledrive_doc_viewer folder and contents in the appropriate 
modules directory of your Drupal installation.  
This is probably sites/all/modules/

2. Enable the googledrive_doc_viewer module in the administration tools. 
It appears in the Modules list as "Google Drive Docs Viewer."
