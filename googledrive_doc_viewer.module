<?php

/**
 * @file
 * A field formatter, which allow add key of docs in google drive.
 */
/**
 * Implements hook_help().
 */
function googledrive_doc_viewer_help($path, $arg) {
  $helptext = '';
  if ($path == 'admin/help#googledrive_doc_viewer') {
    $helptext = '<p>';
    $helptext .= t('This module provides a new format for the Text field type.'.
    ' This format presents set iframe tag and load full content of the docs in google drive on your side' .
    ' i.e. it displays the contents of the text as appropriate to its field' .
    ' (Adobe Acrobat .pdf, Microsoft Word .doc/.docx, Micrososft Excel .xls/.xlsx, Microsoft Powerpoint .ppt/.pptx),' .
    ' using the Google Docs embedded rendering engine.</p>' . 
    '<p>N.B.: Only get field id in your google drive then paste in to the your text field' . 
	' Google Docs must be able to access the file in order to ' .
	' render and display it. In other words, it won\'t work on' . 
	' a typical development laptop, or if your server is behind' .
	' a firewall where Google is unable to access it.');
    $helptext .= '</p>';
    $helptext .= '<p>';
    $helptext .= t('To use this field format, add a Text field to a new or existing content type (such as Basic Page) on the content type\'s Manage Fields form.' .
    ' The Text field type provides only one widget type - Text - so select that. On the content type\'s "Manage Display" form,' .
    ' there will be a drop-down select list of available display formats for the Text field. To display the docs within the embedded' .
    ' Google Drive Docs viewer, choose the \'Google Drive Docs Viewer\' format.');
    $helptext .= '</p>';
    $helptext .= '<p>';
    $helptext .= t('Usage: <br /> - Add file docs to your google drive.<br />- Get file id (0B4qAsjeO6V_8Y09hbWlkS2FKN0k) or get file share url'. 
    ' (https://drive.google.com/open?id=0B4qAsjeO6V_8Y09hbWlkS2FKN0k) then add to the text field.');
    $helptext .= '</p>';
    $helptext .= '<p>';
    $helptext .= t('The document viewer may be styled using the CSS selector \'.google-drive-doc-viewer\'. By default, the viewer\'s width is 100% and its height is 800px.');
    $helptext .= '</p>';
  }
  return $helptext;
}
/**
 * Implements hook_field_formatter_info().
 */
function googledrive_doc_viewer_field_formatter_info() {
   return array(
    'googledrive_doc_viewer' => array(
      'label' => t('Google Drive Docs viewer'),
      'field types' => array('text'),
    ),
  );
}
/**
 * Implements hook_field_formatter_view().
 */
function googledrive_doc_viewer_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'googledrive_doc_viewer':
      foreach ($items as $delta => $item) {
        if (!empty($item)) {
          if(check_url($item['value']) && strpos($item['value'],'https://drive.google.com') !== false){
            $value = str_replace('https://drive.google.com/open?id=', '', trim($item['value']));
          }else{
            $value = check_plain(strip_tags($item['value']));
          } 
          $element[$delta]['#attached']['css'] = array(drupal_get_path('module', 'googledrive_doc_viewer') . '/googledrive.css',);
          $element[$delta]['#markup'] = '<iframe class="google-drive-doc-viewer" scrolling="no" src="https://docs.google.com/viewer?srcid=' . $value . '&amp;pid=explorer&amp;embedded=true"></iframe>';
          
        }
      }
      break;
  }
  return $element;
}
